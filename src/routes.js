//created the router for the view

import Vue from 'vue'
import VueRouter from 'vue-router'

import Hello from './components/Hello.vue'
import Signin from './components/authentication/Signin.vue'
import Login from './components/authentication/Login.vue'
import Dashboard from './components/View/Dashboard.vue'


Vue.use(VueRouter)

const router = new VueRouter({
	routes:[
		{
			path:'/signin',
			component:Signin
		},
		{
			path:'/hello',
			component:Hello
		},
		{
			path:'/login',
			component:Login
		},
		{
			path:'/dashboard',
			component:Dashboard
		}
	]
}) 

export default router